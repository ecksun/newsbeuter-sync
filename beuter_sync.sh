#!/bin/bash

set -e
set -u
set -o pipefail

print_help() {
cat <<EOF
Usage: $(basename "$0") push|pull|sync HOST

Push or pull the newsbeuter read state to or from HOST or synchronize it
between them.
EOF
}

push() {
    local tmp_file
    local dest
    dest="$1"
    tmp_file=$(mktemp)

    # shellcheck disable=SC2064
    trap "rm -f ${tmp_file}" 0 1 2 3 15
    newsbeuter -E "$tmp_file"
    cat "$tmp_file" | ssh "$dest" 'newsbeuter -I <(cat)'
    rm "$tmp_file"
}

pull() {
    local tmp_file
    local dest
    dest="$1"
    tmp_file=$(mktemp)

    # shellcheck disable=SC2029
    newsbeuter -I <(ssh -T "$dest" "$(cat <<"EOF"
    set -e
    set -u
    set -o pipefail
    tmp_file=$(mktemp)

    trap "rm -f ${tmp_file}" 0 1 2 3 15
    newsbeuter -E "$tmp_file" >&2
    cat "$tmp_file"
EOF
)")
}

reload_local() {
    newsbeuter -x reload
}

reload_remote() {
    local dest
    dest="$1"
    ssh "$dest" newsbeuter -x reload
}

if [ "$#" -ne 2 ]; then
    print_help
    exit 127
fi

if [ "$1" == "push" ]; then
    push "$2"
elif [ "$1" == "pull" ]; then
    pull "$2"
elif [ "$1" == "sync" ]; then
    echo "Reloading local instance"
    reload_local
    echo "Reloading remote instance"
    reload_remote "$2"
    echo "Pushing read state"
    push "$2"
    echo "Pulling read state"
    pull "$2"
else
    print_help
    exit 127
fi
